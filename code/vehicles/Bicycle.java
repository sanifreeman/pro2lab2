package vehicles;
public class Bicycle{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle(String man, int numg, double maxs){
        this.manufacturer=man;
        this.numberGears=numg;
        this.maxSpeed=maxs;
    }
    public String getManufacturer(){
        return this.manufacturer;
    }
    public int getNumGears(){
    return this.numberGears;
    } 
    public double getmaxSpeed(){
    return this.maxSpeed;
    }
    public String toString(){
        String bike= "the manufacturer is "+this.manufacturer+"  The number of gears is:"+this.numberGears+" the maxSpeed is: "+this.maxSpeed+".";
        return bike;
    }

}